
import json
import os
from pathlib import Path
import time
from types import SimpleNamespace

from authlib.integrations.requests_client import OAuth2Session
from authlib.oauth2.rfc7523 import PrivateKeyJWT
from ipywidgets_jsonschema import Form


object_hook = lambda d: SimpleNamespace(**d)


def jobyter(template, machine, config_file=None):

    config_file = config_file or Path(os.environ["HOME"]) / "api-config.json"

    with open(config_file, "r") as f:
        config = json.load(f)

    session = OAuth2Session(
        config["client_id"],
        config["private_key"],
        PrivateKeyJWT(config["token_url"]),
        grant_type="client_credentials",
        token_endpoint=config["token_url"]
    )
    token = session.fetch_token()

    return JobProxy(session, config["api_url"], template, machine)


class TimeoutError(Exception):
    pass


class UnexpectedJobStateError(Exception):
    pass


class JobProxy:

    def __init__(self, session, base_url, template, machine):
        self.session = session
        self.base_url = base_url
        self.template = template
        self.machine = machine
        self._form = Form(self.get_schema()[0]["schema"])

        self.template_response = None
        self.job_id = None

    def form(self, **kwargs):
        self._form.data = kwargs
        self._form.show()

    @property
    def task_id(self):
        return self.template_response.command_output.task_id

    @property
    def variables(self):
        return self.template_response.variables

    def submit_and_wait(
        self,
        check_timeout=600,
        check_interval=10,
        check_delay=0
    ):

        # Submit template request, response should include task

        self.template_response = self.post_template(self._form.data)
        print(f"Submitted: task_id = {self.task_id}")

        # Start clock and delay check if requested

        start = time.time()
        print(f"Waiting {check_delay} s before checking for job ID")
        time.sleep(check_delay)

        # Check task for job ID

        print("Checking for job ID")

        task_data = None
        while True:
            task_data = self.get_task()
            if task_data.status == "completed":
                print()
                break
            elapsed = time.time() - start
            print(f"Elapsed time (s): {int(elapsed)}", end="\r")
            if check_timeout and elapsed > check_timeout:
                raise TimeoutError("Request timed out, never got job ID")
            time.sleep(check_interval)

        # Extract job ID

        try:
            task_result = json.loads(task_data.result, object_hook=object_hook)
            self.job_id = task_result.jobid
        except:
            print(f"Error: {task_data=}")
            raise
        print(f"Job ID = {self.job_id}")

        # Poll job until it is running

        print("Checking for job start")

        try:
            while True:
                if self.check_job():
                    print()
                    print(" *** Running *** ")
                    return
                elapsed = time.time() - start
                print(f"Elapsed time (s): {int(elapsed)}", end="\r")
                if check_timeout and elapsed > check_timeout:
                    self.delete_job()
                    raise TimeoutError("Job request timed out")
                time.sleep(check_interval)
        except KeyboardInterrupt:
            self.delete_job()
            raise

    def check_job(self):
        job_data = self.get_job()
        if job_data.output:
            if job_data.output[0].state == "RUNNING":
                return True
            if job_data.output[0].state == "PENDING":
                return False
            self.delete_job()
            raise UnexpectedJobStateError(
                f"Unexpected job state: {job_data.output[0].state}"
            )
        else:
            raise UnexpectedJobStateError(f"No output: {job_data=}")

    def get_schema(self):
        return self._request("GET", self.schema_url, hook=None)

    def post_template(self, data={}, test=False):
        url = self.template_url
        url += "?test=1" if test else ""
        return self._request("POST", url, data)

    def get_task(self):
        return self._request("GET", self.task_url)

    def get_job(self):
        return self._request("GET", self.job_url)

    def delete_job(self):
        return self._request("DELETE", self.job_url)

    def _request(self, method, url, data=None, hook=object_hook):
        r = self.session.request(method, url, json=data)
        r.raise_for_status()
        return r.json(object_hook=hook)

    @property
    def schema_url(self):
        return (
            f"{self.base_url}/compute/templates?template={self.template}&machine={self.machine}"
        )

    @property
    def template_url(self):
        return (
            f"{self.base_url}/compute/templates/{self.template}/{self.machine}"
        )

    @property
    def task_url(self):
        return f"{self.base_url}/tasks/{self.task_id}"

    @property
    def job_url(self):
        return f"{self.base_url}/compute/jobs/{self.machine}/{self.job_id}"
