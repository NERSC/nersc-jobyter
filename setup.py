
from setuptools import setup, find_packages

setup(
    author="R. C. Thomas",
    author_email="rcthomas@lbl.gov",
    description="Templated NERSC jobs from your Jupyter notebook",
    name="nersc-jobyter",
    version="0.0.4",
    packages=find_packages(exclude=["tests"]),
)
